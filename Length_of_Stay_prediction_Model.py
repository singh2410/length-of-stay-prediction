#!/usr/bin/env python
# coding: utf-8

# # Length of Stay prediction Model 
# #By- Aarush Kumar
# #Dated: July 15,2021

# ## About- Length of Stay prediction:
#  A term defined by the NHS as the length of an inpatient episode of care, calculated from the day of admission to day of discharge, and based on the number of nights spent in hospital. Patients admitted and discharged on the same day have a length of stay of less than one day.

# In[2]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
get_ipython().run_line_magic('matplotlib', 'inline')
sns.set_style("whitegrid")
plt.style.use("fivethirtyeight")


# In[3]:


train = pd.read_csv('/home/aarush100616/Downloads/Projects/Length of Stay Prediction Model/train_data.csv')
test = pd.read_csv('/home/aarush100616/Downloads/Projects/Length of Stay Prediction Model/test_data.csv')
dictionary = pd.read_csv('/home/aarush100616/Downloads/Projects/Length of Stay Prediction Model/train_data_dictionary.csv')
sample = pd.read_csv('/home/aarush100616/Downloads/Projects/Length of Stay Prediction Model/sample_sub.csv')


# In[4]:


dictionary


# ## Exploratory Data Analysis (EDA)

# In[5]:


train.info()


# In[6]:


train


# In[7]:


train.Stay.value_counts()


# In[8]:


# Distribution of target feature
plt.figure(figsize=(10,7))
train.Stay.value_counts().plot(kind="bar", color = ['Salmon'])


# In[9]:


# Check for unique values in every column
for features in train.columns:
    print('Unique Values for {}'.format(features))
    print(train[features].unique())
    print('======================================')
    print()


# In[10]:


# Check for null values
train.isna().sum()


# ## Data Processing & Feature engineering

# In[11]:


train = train.drop(['Hospital_region_code', 'Bed Grade', 'patientid', 'City_Code_Patient'], axis = 1)
test = test.drop(['Hospital_region_code', 'Bed Grade', 'patientid', 'City_Code_Patient'], axis = 1)


# In[12]:


# Combine test and train dataset for processing
combined = [train, test]


# In[13]:


from sklearn.preprocessing import LabelEncoder
for dataset in combined:
    label = LabelEncoder()
    dataset['Department'] = label.fit_transform(dataset['Department'])


# In[14]:


combined[1].Department.unique()


# In[15]:


# Ward type
for dataset in combined:
    label = LabelEncoder()
    dataset['Hospital_type_code'] = label.fit_transform(dataset['Hospital_type_code'])
    dataset['Ward_Facility_Code'] = label.fit_transform(dataset['Ward_Facility_Code'])
    dataset['Ward_Type'] = label.fit_transform(dataset['Ward_Type'])
    dataset['Type of Admission'] = label.fit_transform(dataset['Type of Admission'])
    dataset['Severity of Illness'] = label.fit_transform(dataset['Severity of Illness'])


# In[16]:


combined[0]


# In[17]:


combined[1]


# In[18]:


# Check age distribution
combined[0].Age.hist()


# In[19]:


combined[0].Age.unique()


# In[20]:


age_dict = {'0-10': 0, '11-20': 1, '21-30': 2, '31-40': 3, '41-50': 4, '51-60': 5, '61-70': 6, '71-80': 7, '81-90': 8, '91-100': 9}


# In[21]:


for dataset in combined:
    dataset['Age'] = dataset['Age'].replace(age_dict.keys(), age_dict.values())


# In[22]:


combined[0].Stay.unique()


# In[23]:


stay_dict = {'0-10': 0, '11-20': 1, '21-30': 2, '31-40': 3, '41-50': 4, '51-60': 5, '61-70': 6, '71-80': 7, '81-90': 8, '91-100': 9, 'More than 100 Days': 10}


# In[24]:


combined[0]['Stay'] = combined[0]['Stay'].replace(stay_dict.keys(), stay_dict.values())


# In[25]:


combined[0].Age.hist()


# In[26]:


for dataset in combined:
    print(dataset.shape)


# In[27]:


combined[1].info()


# In[28]:


columns_list = ['Type of Admission', 'Available Extra Rooms in Hospital', 'Visitors with Patient', 'Admission_Deposit']


# In[29]:


len(columns_list)


# In[30]:


from sklearn.preprocessing import StandardScaler
ss= StandardScaler()
for dataset in combined:
    dataset[columns_list]= ss.fit_transform(dataset[columns_list].values)


# In[31]:


combined[0]


# In[32]:


plt.figure(figsize=(12,12))
sns.heatmap(combined[0].corr(), annot=True, cmap='coolwarm')


# ## Data Modelling

# In[33]:


from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier


# In[34]:


train = combined[0]
test = combined[1]


# In[35]:


sample


# In[36]:


X_train = train.drop(['case_id', 'Stay'], axis=1)
Y_train = train["Stay"]
X_test  = test.drop("case_id", axis=1).copy()
X_train.shape, Y_train.shape, X_test.shape


# In[37]:


sample.shape


# In[38]:


X_test.columns


# In[39]:


Y_train


# In[40]:


# KNN
knn = KNeighborsClassifier(n_neighbors = 3)
knn.fit(X_train, Y_train)
Y_pred = knn.predict(X_test)
acc_knn = round(knn.score(X_train, Y_train) * 100, 2)
acc_knn


# In[41]:


# Decision Tree
decision_tree = DecisionTreeClassifier()
decision_tree.fit(X_train, Y_train)
Y_pred = decision_tree.predict(X_test)
acc_decision_tree = round(decision_tree.score(X_train, Y_train) * 100, 2)
acc_decision_tree


# In[42]:


# Random Forest
random_forest = RandomForestClassifier(n_estimators=100)
random_forest.fit(X_train, Y_train)
Y_pred = random_forest.predict(X_test)
random_forest.score(X_train, Y_train)
acc_random_forest = round(random_forest.score(X_train, Y_train) * 100, 2)
acc_random_forest


# In[43]:


sns.barplot(x= ['KNN','DT','RF'],y= [acc_knn, acc_decision_tree,acc_random_forest])

